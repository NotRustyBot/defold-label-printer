# Defold Label Printer  
---  

Here you can find some useful code to create colorful text with labels.  
Read more on the [Defold Forum](https://forum.defold.com/t/labels-with-highlighted-words/65389)  
Feel free to message me (davidgrabeuk)  

---  

This particular branch has the printer set up in away to highlight individual words. The *$* symbol is used right before the word to highlight it.  

![image](image.png)  

Message used to produce this text:  

*msg.post("printer", "text", {text = "This text is $highlighted", width = 200})*  

---